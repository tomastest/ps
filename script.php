<?php
use CommissionFeesCalculator\Classes\Calculator;
use CommissionFeesCalculator\Classes\Validator;

require_once 'app/start.php';

Validator::isArgumentsPassed($argc);
$fileLocation = $argv[1];
Validator::isFileReadable($fileLocation);
Validator::isFileEmpty($fileLocation);

$rowsOfData = file($fileLocation);

foreach ($rowsOfData as $row) {
    list($date, $customerId, $customerType, $operationType, $amount, $currency) = explode(",", trim($row));

    $fee = Calculator::getFee($date, $customerId, $customerType, $operationType, $amount, $currency);

    fwrite(STDOUT, $fee."\n");
}
