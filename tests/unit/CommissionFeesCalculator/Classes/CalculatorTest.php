<?php

use PHPUnit\Framework\TestCase;
use CommissionFeesCalculator\Classes\Calculator;

class CalculatorTest extends TestCase
{
    public function testCashInFee()
    {
        $fee = Calculator::cashInFee(1000, 'EUR');
        $this->assertEquals(0.3, $fee);
    }

    public function testGetCashOutFeeForNaturalCustomer()
    {
        $row = "2004-12-31,4,natural,cash_out,1200,EUR";

        list($date, $customerId, $customerType, $operationType, $amount, $currency) = explode(",", trim($row));

        $fee = Calculator::getFee($date, $customerId, $customerType, $operationType, $amount, $currency);

        $this->assertEquals(0.6, $fee);
    }
    public function goodData()
    {
        return [
            ["2014-12-31,4,natural,cash_out,1200,EUR",0.6],
            ["2015-01-01,4,natural,cash_out,1000,EUR",3],
            ["2015-01-01,4,natural,cash_out,2000,EUR",6],
            ["2019-01-10,2,legal,cash_in,1000000,EUR",5],
            ["2020-01-06,2,legal,cash_out,300.00,EUR",0.9]
        ];
    }

    /**
     * @dataProvider goodData
     */
    public function testGetFeeForSeveralRows($dataRow, $correctFee)
    {
        list($date, $customerId, $customerType, $operationType, $amount, $currency) = explode(",", trim($dataRow));

        $fee = Calculator::getFee($date, $customerId, $customerType, $operationType, $amount, $currency);

        $this->assertEquals($correctFee, $fee);
    }

    public function corruptedData()
    {
        return [
            ["2014-12-31,4,natural,cash_out,1200,E"],
            ["2014-12-31,4,natural,cash_out,1200,EU"],
            ["2014-12-31,4,natural,cash_out,1200,RUB"],
            ["2014-12-31,4,natural,cash_out,1200,1"],
            ["2014-12-31,4,natural,cash_out,1200,0"],
            ["2014-12-31,4,natural,cash_out,1200,@"],
            ["2014-12-31,4,natural,cash_out,1200,"],
            ["2014-12-31,4,natural,cash_out,0,JPY"],
            ["2014-12-31,4,natural,cash_out,-1,USD"],
            ["2014-12-31,4,natural,cash_out,x,USD"],
            ["2014-12-31,4,natural,cash_outt,10,USD"],
            ["2014-12-31,4,natural,0,10,USD"],
            ["2014-12-31,4,natural,32r3refds,10,USD"],
            ["2014-12-31,4,naturall,cash_in,10,USD"],
            ["2014-12-31,4,llegal,cash_in,10,USD"],
            ["2014-12-31,4,0,cash_in,10,USD"],
            ["2014-12-31,4,x,cash_in,10,USD"],
            ["2014-12-31,x,legal,cash_in,10,USD"],
            ["2014-12-31,-1,legal,cash_in,10,USD"],
            ["2014-12311,1,legal,cash_in,10,USD"],
            ["a241231,1,legal,cash_in,10,USD"],
            ["x-201s41231,1,legal,cash_in,10,USD"],
            [",,,,,,,,,,,,,"],
            [",,,,,wergrfrDg8987"],
        ];
    }

    /**
     * @dataProvider corruptedData
     */
    public function testThrowsExceptionWhenDataIsInvalid($dataRow)
    {
        $this->expectException(\InvalidArgumentException::class);

        list($date, $customerId, $customerType, $operationType, $amount, $currency) = explode(",", trim($dataRow));

        Calculator::getFee($date, $customerId, $customerType, $operationType, $amount, $currency);
    }
}
