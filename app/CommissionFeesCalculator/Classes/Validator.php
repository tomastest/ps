<?php

namespace CommissionFeesCalculator\Classes;

class Validator
{
    public static function isFileReadable(string $fileLocation)
    {
        if (!is_readable($fileLocation)) {
            throw new \Exception("File doesn't exist in this location or doesn't have a reading permission.\n");
        }
    }

    public static function isFileEmpty(string $fileLocation)
    {
        if (!filesize($fileLocation)) {
            throw new \Exception("File is empty.\n");
        }
    }

    public static function isArgumentsPassed($args)
    {
        if ($args !== 2) {
            throw new \Exception("Must be passed only 2 arguments.\n");
        }
    }

    // The given amount must be numeric and higher than zero.
    public static function isAmountValid(string $amount)
    {
        if (!is_numeric($amount) || $amount <= 0) {
            throw new \InvalidArgumentException;
        }
    }

    // Checking if date is formatted in "Y-m-d"
    public static function isDateValid(string $date)
    {
        if (date('Y-m-d', strtotime($date)) !== $date) {
            throw new \InvalidArgumentException;
        }
    }

    public static function isCustomerIdValid(string $customerId)
    {
        if (!is_numeric($customerId) || $customerId < 0) {
            throw new \InvalidArgumentException;
        }
    }

    public static function isRateAvailable(string $conversionType, array $rates)
    {
        if (!array_key_exists($conversionType, $rates)) {
            throw new \InvalidArgumentException;
        }
    }

    public static function isCurrencyAvailable(string $currency, array $allCurrencies)
    {
        if (!in_array($currency, $allCurrencies)) {
            throw new \InvalidArgumentException;
        }
    }

    public static function isOperationTypeValid(string $operation, array $operationTypes)
    {
        if (!in_array($operation, $operationTypes)) {
            throw new \InvalidArgumentException;
        }
    }

    public static function isCustomerTypeValid(string $customer, array $customerTypes)
    {
        if (!in_array($customer, $customerTypes)) {
            throw new \InvalidArgumentException;
        }
    }
}
