<?php

namespace CommissionFeesCalculator\Classes;

class Calculator
{
    public static function getFee($date, $customerId, $customerType, $operationType, $amount, $currency)
    {
        Validator::isDateValid($date);
        Validator::isCustomerIdValid($customerId);
        Validator::isCustomerTypeValid($customerType, Data::CUSTOMER_TYPES);
        Validator::isOperationTypeValid($operationType, Data::OPERATION_TYPES);
        Validator::isAmountValid($amount);
        Validator::isCurrencyAvailable($currency, Data::AVAILABLE_CURRENCIES);

        $fee = 0;
        if ($operationType === 'cash_in') {
            $fee = self::cashInFee($amount, $currency);
        }
        if ($operationType === 'cash_out') {
            if ($customerType === 'legal') {
                $fee = self::cashOutFeeLegalCustomers($amount, $currency);
            }
            if ($customerType === 'natural') {
                $fee =  self::cashOutFeeNaturalCustomers($date, $customerId, $amount, $currency);
            }
        }
        return $fee;
    }

    public static function cashInFee($amount, $currency)
    {
        $fee = $amount * Data::CASH_IN_FEE;

        if ($fee > Data::convert(Data::CASH_IN_MAX_FEE, Data::DEFAULT_CURRENCY, $currency)) {
            $fee = Data::convert(Data::CASH_IN_MAX_FEE, Data::DEFAULT_CURRENCY, $currency);
        }

        return Data::roundUp($fee);
    }

    public static function cashOutFeeLegalCustomers($amount, $currency)
    {
        $fee = $amount * Data::CASH_OUT_FEE;

        if ($fee < Data::convert(Data::CASH_OUT_MIN_FEE, Data::DEFAULT_CURRENCY, $currency)) {
            $fee = Data::convert(Data::CASH_OUT_MIN_FEE, Data::DEFAULT_CURRENCY, $currency);
        }

        return Data::roundUp($fee);
    }

    public static function cashOutFeeNaturalCustomers($date, $customerId, $amount, $currency)
    {
        $weeklyStampId = date('oW', strtotime($date)) . $customerId;

        if (!Trx::existStamp($weeklyStampId)) {
            $feeObj = (new Trx($weeklyStampId));
        } else {
            $feeObj = Trx::$existingData[$weeklyStampId];
        }

        return $feeObj->cashOut($amount, $currency);
    }
}
