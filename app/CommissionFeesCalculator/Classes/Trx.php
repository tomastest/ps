<?php


namespace CommissionFeesCalculator\Classes;

class Trx
{
    private $weeklyCounter;
    private $cashOutAllowance;
    private $fee;
    public static $existingData = [];

    public static function existStamp($stamp)
    {
        $allStamps = array_keys(self::$existingData);
        return in_array($stamp, $allStamps);
    }

    public function __construct($stamp)
    {
        $this->cashOutAllowance = Data::CASH_OUT_WEEKLY_LIMIT;
        self::$existingData[$stamp] = $this;

        return $this;
    }

    public function exceedsLimits()
    {
        return (
            $this->weeklyCounter >= Data::CASH_OUT_WEEKLY_TIMES_LIMIT ||
            $this->cashOutAllowance[Data::DEFAULT_CURRENCY] <= 0
        );
    }

    public function cashOut($amount, $currency)
    {
        if ($this->exceedsLimits()) {
            $this->fee = Data::roundUp(Data::CASH_OUT_FEE * $amount);
        } else {
            $this->weeklyCounter++;
            foreach ($this->cashOutAllowance as $iCurrency => &$iAmount) {
                $iAmount -= Data::convert($amount, $currency, $iCurrency);
            }

            $this->fee = 0;
            if ($this->cashOutAllowance[$currency] < 0) {
                $this->fee = Data::roundUp(-Data::CASH_OUT_FEE * $this->cashOutAllowance[$currency]);
            }
        }
        return $this->fee;
    }

}
