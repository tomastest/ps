<?php
namespace CommissionFeesCalculator\Classes;

class Data
{
    const CUSTOMER_TYPES = ['legal','natural'];
    const OPERATION_TYPES = ['cash_in', 'cash_out'];
    const AVAILABLE_CURRENCIES = ['EUR', 'USD', 'JPY'];

    // Cash in fees or all customers
    const CASH_IN_FEE = 0.0003;
    const CASH_IN_MAX_FEE = 5;
    const DEFAULT_CURRENCY = 'EUR';

    // For natural customers
    const CASH_OUT_WEEKLY_LIMIT = [
        'EUR'   => 1000,
        'USD'   => 1000 * self::USD_CONVERSION_RATE,
        'JPY'   => 1000 * self::JPY_CONVERSION_RATE
    ];
    const CASH_OUT_WEEKLY_TIMES_LIMIT = 3;

    // For legal customers
    const CASH_OUT_FEE = 0.003;// and for natural customers also
    const CASH_OUT_MIN_FEE = 0.5;

    // Conversion rates
    const USD_CONVERSION_RATE = 1.1497;// EUR:USD 1:1.1497
    const JPY_CONVERSION_RATE = 129.53;// EUR:JPY 1:129.53

    const RATES = [
        'EURUSD'    => self::USD_CONVERSION_RATE,
        'USDEUR'    => 1/self::USD_CONVERSION_RATE,
        'EURJPY'    => self::JPY_CONVERSION_RATE,
        'JPYEUR'    => 1/self::JPY_CONVERSION_RATE,
        'USDJPY'    => 1/self::USD_CONVERSION_RATE * self::JPY_CONVERSION_RATE,
        'JPYUSD'    => 1/self::JPY_CONVERSION_RATE * self::USD_CONVERSION_RATE,
        'EUREUR'    => 1,
        'USDUSD'    => 1,
        'JPYJPY'    => 1
    ];

    public static function convert($amount, $from, $to)
    {
        $conversionType = "$from$to";
        Validator::isRateAvailable($conversionType, self::RATES);

        return $amount * self::RATES[$conversionType];
    }

    // Rounds up the amount to the precision of its 100th part.
    public static function roundUp($amount)
    {
        return ceil(($amount)*100)/100;
    }

}

